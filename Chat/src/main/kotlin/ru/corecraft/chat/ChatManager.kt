package ru.corecraft.chat

import net.md_5.bungee.api.ChatMessageType
import net.md_5.bungee.api.chat.TextComponent
import net.milkbowl.vault.chat.Chat
import org.bukkit.Bukkit
import org.bukkit.Sound
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import ru.corecraft.chat.extensions.prefix
import ru.corecraft.chat.extensions.suffix

object ChatManager : Listener{
    private lateinit var main: ChatPlugin
    lateinit var chat: Chat
    val messageList =
            HashMap<Player, Player>()

    fun init(chatPlugin: ChatPlugin, chat: Chat){
        this.main = chatPlugin
        this.chat = chat
    }
    fun get(): ChatManager = this

    @EventHandler
    fun onChat(e: AsyncPlayerChatEvent) {
        e.isCancelled = true
        if(e.message.startsWith("!")) {
            e.message = e.message.trim().substring(1)
            for (p in Bukkit.getOnlinePlayers()) {
                if (e.message.contains(p.name)) {
                    e.message = e.message.replace(p.name, "§e${p.name}§f")
                    p.spigot().sendMessage(
                            ChatMessageType.ACTION_BAR,
                            *TextComponent.fromLegacyText("§aВы были упомянуты в чате!")
                    )
                    p.playSound(p.location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 100f, 50f)
                }
            }
            val toSend = "§8[§6G§8] §r${e.player.prefix}${e.player.name}${e.player.suffix} §8» §f${e.message}".replace("&", "§")
            for (p in Bukkit.getOnlinePlayers()) {
                p.sendMessage(toSend)
            }
            main.logger.info(toSend)
        }else {
            for (p: Player in Bukkit.getOnlinePlayers()){
                if(p.location.distance(e.player.location) < 100){
                    if (e.message.contains(p.name)) {
                        e.message = e.message.replace(p.name, "§e${p.name}§7§o")
                        p.spigot().sendMessage(
                                ChatMessageType.ACTION_BAR,
                                *TextComponent.fromLegacyText("§aВы были упомянуты в чате!")
                        )
                        p.playSound(p.location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 100f, 50f)
                    }
                    val toSend = "§8[§7L§8] §r${e.player.prefix+e.player.name+e.player.suffix} §8» §7§o${e.message}".replace("&", "§")
                    p.sendMessage(toSend)
                    main.logger.info(toSend)
                }
            }
        }
    }

    fun addMessage(get: Player, send: Player) { messageList[get] = send }


    fun removeMessage(get: Player, send: Player) { messageList.remove(get, send) }
}