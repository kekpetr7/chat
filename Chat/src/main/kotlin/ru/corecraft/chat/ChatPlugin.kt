package ru.corecraft.chat

import net.milkbowl.vault.chat.Chat
import org.bukkit.Bukkit
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin
import ru.corecraft.chat.commands.MsgCommand
import ru.corecraft.chat.commands.PrefixCommand
import ru.corecraft.chat.commands.ReMessageCommand

class ChatPlugin : JavaPlugin(), Listener {
    lateinit var chat: Chat

    override fun onEnable() {
        if (setupChat()) {
            ChatManager.init(this, chat)
            Bukkit.getPluginManager().registerEvents(ChatManager.get(), this)
            getCommand("r")?.setExecutor(ReMessageCommand())
            getCommand("msg")?.setExecutor(MsgCommand())
            getCommand("prefix")?.setExecutor(MsgCommand())
        }
    }

    private fun setupChat(): Boolean {
        val rsp =
            server.servicesManager.getRegistration(
                Chat::class.java
            )
                ?: return false
        chat = rsp.provider
        return rsp != null
    }
}