package ru.corecraft.chat.commands

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import ru.corecraft.chat.ChatManager

class MsgCommand : CommandExecutor {

    private val usage = "/msg <игрок> <сообщение>"

    override fun onCommand(
        sender: CommandSender,
        command: Command,
        label: String,
        args: Array<String>
    ): Boolean {
        if (args.size < 2) {
            sender.sendMessage("§cНеверное использование!")
            sender.sendMessage("§cИспользование: $usage")
            return false
        }
        val p = Bukkit.getPlayer(args[0])
        if (p == null) {
            sender.sendMessage("§cИгрок офлайн!")
            sender.sendMessage("§cИспользование: $usage")
        }
        if (ChatManager.messageList.containsKey(sender as Player)) {
            val pll: Player? = ChatManager.messageList[sender]
            ChatManager.removeMessage(sender, pll!!)
            ChatManager.removeMessage(pll, sender)
        }
        val message = args.copyOfRange(1, args.size).joinToString(" ").replace("&", "§")
        ChatManager.addMessage(p!!, sender)
        ChatManager.addMessage(sender, p)
        sender.sendMessage("§8[§cЯ §8-> §c${p.displayName}§8]§f $message")
        p.sendMessage("§8[§c${sender.displayName} §8-> §cЯ§8] §f$message")
        return true
    }

}