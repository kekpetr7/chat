package ru.corecraft.chat.commands

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import ru.corecraft.chat.ChatManager
import ru.corecraft.chat.extensions.prefix

class PrefixCommand : CommandExecutor {

    private val usage = "/prefix <игрок> <префикс> ИЛИ /prefix group <группа> <префикс>"

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if(args?.size!! < 2){
            sender?.sendMessage("§cНедостаточно аргументов!")
            sender?.sendMessage("§cИспользование: $usage")
            return false
        }
        if(args[0] == "group") return onGroupCommand(sender, args)
        val p = Bukkit.getPlayer(args[0])
        if(p == null){
            sender?.sendMessage("Игрок оффлайн!")
            sender?.sendMessage("§cИспользование: $usage")
            return false
        }
        val prefix = args.copyOfRange(1, args.size).joinToString(" ").replace("&", "§")
        p.prefix = prefix
        sender?.sendMessage("§aПрефикс игрока ${p.name} изменён на $prefix")
        return true
    }

    private fun onGroupCommand(sender: CommandSender?, args: Array<out String>?): Boolean{
        if (args?.size!! < 3){
            sender?.sendMessage("§cНедостаточно аргументов!")
            sender?.sendMessage("§cИспользование: $usage")
            return false
        }
        for(group in ChatManager.get().chat.groups){
            if(args[1] == group){
                val prefix = args.copyOfRange(2, args.size).joinToString(" ").replace("&", "§")
                for(world in Bukkit.getWorlds())
                    ChatManager.get().chat.setGroupPrefix(world, group, prefix)
                sender?.sendMessage("§cГруппе $group установлен префикс $prefix")
                return true
            }
        }
        return false
    }
}