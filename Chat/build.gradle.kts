plugins {
    kotlin("jvm") version "1.3.72"
}

group = "ru.corecraft.chat"
version = "1.0.0-SNAPSHOT"

tasks {
    jar {
        doFirst {
            from({
                configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }
            })
        }
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    maven {
        url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
        content {
            includeGroup("org.bukkit")
            includeGroup("org.spigotmc")
        }
    }
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
        name = "bungeecord-repo"
    }
    maven { url = uri("https://jitpack.io") }
}

dependencies {
    compileOnly("net.md-5:bungeecord-chat:1.12-SNAPSHOT")
    compileOnly("org.bukkit:bukkit:1.12.2-R0.1-SNAPSHOT") // The Bukkit API with no shadowing.
    compileOnly("org.spigotmc:spigot-api:1.12.2-R0.1-SNAPSHOT")
    compileOnly("com.github.MilkBowl:VaultAPI:1.7")
    implementation(kotlin("stdlib-jdk8"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}